<html>
  <head>
    <?php
    include("partials/_head.php")
    ?>
  </head>
  <body>
    <!-- comienza header -->
      <?php
      include("partials/_header.php")
      ?> 
    <!-- termina header -->
    <div class="sect sect--padding-top">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="site">
              <img class="site__img" src="public/images/isiukak.jpg">
              <h1 class="site__title">Think outside the blocks.</h1>
              <h2 class="site__subtitle">Vote for Block Producer</h2>
              <div class="site__box-link">
                <a class="btn btn--width" href="">Vote</a>
                <a class="btn btn--revert btn--width" href="">Telegram</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="sect sect--padding-bottom">
      <div class="container">
        <div class="row row--center">
          <h1 class="row__title">
            Technology 
          </h1>
          <h2 class="row__sub">What makes us the best?</h2>
        </div>
        <div class="row row--center row--margin">
          <div class="col-md-4 col-sm-4 price-box price-box--purple">
            <div class="price-box__wrap">
              <div class="price-box__img"></div>
              <h1 class="price-box__title">
                Company Location
              </h1>
              <p class="price-box__people">
                Spain
              </p>
              <h2 class="price-box__discount">
                <span class="price-box__dollar"></span>Server Locations<span class="price-box__discount--light"></span>
              </h2>
              <h3 class="price-box__price">
                We plan to host servers in secure, private data centers around the world. These are the initial nodes
              </h3>
              <p class="price-box__feat">
                Initial nodes
              </p>
              <ul class="price-box__list">
                <li class="price-box__list-el">May 2018 - Spain and Iceland - Testnet Nodes</li>
                <li class="price-box__list-el">June 2018 - Iceland - Mainnet Nodes</li>
                <li class="price-box__list-el">Q3 2018 - Additional Node in North America</li>
                <li class="price-box__list-el">Q4 2018 - Additional Node in South America</li>
              </ul>
              <div class="price-box__btn">
                <a class="btn btn--purple btn--width">Start now</a>
              </div>
            </div>
          </div>
          <!-- second -->
          <div class="col-md-4 col-sm-4 price-box price-box--violet">
            <div class="price-box__wrap">
              <div class="price-box__img"></div>
              <h1 class="price-box__title">
                Servers
              </h1>
              <p class="price-box__people">
                HP Enterprise ProLiant DL380 Gen 9 E5-2660v4
              </p>
              <h2 class="price-box__discount">
              <span class="price-box__dollar">$</span>149<span class="price-box__discount--light">/mo</span>
              </h2>
              <h3 class="price-box__price">
                Estimate of technical specifications and total expenditure for resources by June 3, 2018
              </h3>
              <p class="price-box__feat">
                Features
              </p>
              <ul class="price-box__list">
                <li class="price-box__list-el">HP</li>
                <li class="price-box__list-el">WAN Load balancer</li>
                <li class="price-box__list-el">Firewall</li>
                <li class="price-box__list-el"> 1GB Internet lines (x2)</li>
              </ul>
              <div class="price-box__btn">
                <a class="btn btn--violet btn--width">Start now</a>
              </div>
            </div>
          </div>

          <!-- terzo -->
          <div class="col-md-4 col-sm-4 price-box price-box--blue">
            <div class="price-box__wrap">
              <div class="price-box__img"></div>
              <h1 class="price-box__title">
                Hardware
              </h1>
              <p class="price-box__people">
                500+ people
              </p>
              <h2 class="price-box__discount">
              <span class="price-box__dollar">$</span>399<span class="price-box__discount--light">/mo</span>
              </h2>
              <h3 class="price-box__price">
                $499/mo
              </h3>
              <p class="price-box__feat">
                Features
              </p>
              <ul class="price-box__list">
                <li class="price-box__list-el">2 x Cisco ASA 5500 Firewall</li>
                <li class="price-box__list-el">2 x Barracuda ADC Load Balancer</li>
                <li class="price-box__list-el">HPE ProLiant rack server - Main Cluster - ex: Ubuntu 16.10 server for and backup main producer node, backup producer node, full EOS node</li>
                <li class="price-box__list-el">HPE ProLiant DL380 Gen9 E5-2660v4 with Intel Xeon E5-2660 v4 processor, memory 3.0 TB with 512GB DDR4, 2 x 500GB SSD hard disk</li>
                <li class="price-box__list-el">HPE ProLiant DL380 Gen9 E5-2660v4 with Intel Xeon E5-2660 v4 processor, memory 1.0 TB with 512GB DDR4, 2x 500GB SSD Hard disk</li>
              </ul>
              <div class="price-box__btn">
                <a class="btn btn--blue btn--width">Start now</a>
              </div>
            </div>
          </div> 
        </div>
      </div>
    </div>


    <div class="sect sect--white">
      <div class="container">
        <div class="row">
          <h1 class="row__title">
            Join our Community
          </h1>
          
          <div class="sect sect--white sect--no-padding">
            <div class="container">
              <div class="row row--center">
                <div class="col-md-3 col-xs-6 col-sm-6 partner">
                  <a href="#" class="partner__link">
                  <img class="partner_img" src="https://image.ibb.co/mOtHRw/fblogo.png">
                  </a>
                </div>
                
                <div class="col-md-3  col-xs-6 col-sm-6 partner">
                  <a href="#" class="partner__link">
                  <img class="partner_img" src="https://image.ibb.co/nfpXRw/twitterlogo.png">
                  </a>
                </div>
                
                <div class="col-md-3 col-xs-6 col-sm-6 partner">
                  <a href="#" class="partner__link">
                  <img class="partner_img" src="https://image.ibb.co/imgOYb/googlelogo.png">
                  </a>
                </div>
                
                <div class="col-md-3 col-xs-6 col-sm-6 partner">
                  <a href="#" class="partner__link">
                  <img class="partner_img" src="https://image.ibb.co/ebGAeG/dribbblelogo.png">
                  </a>
                </div>
              </div>
              <div class="row row--center">
                <div class="col-md-3 col-xs-6 col-sm-6 partner">
                  <a href="#" class="partner__link">
                  <img class="partner_img" src="https://image.ibb.co/npV8Yb/gitlogo.png">
                  </a>
                </div>
                
                <div class="col-md-3 col-xs-6 col-sm-6 partner">
                  <a href="#" class="partner__link">
                  <img class="partner_img" src="https://image.ibb.co/cGyZ6w/stacklogo.png">
                  </a>
                </div>
                
                <div class="col-md-3 col-xs-6 col-sm-6 partner">
                  <a href="#" class="partner__link">
                  <img class="partner_img" src="https://image.ibb.co/ij03zG/inlogo.png">
                  </a>
                </div>
                
                <div class="col-md-3 col-xs-6 col-sm-6 partner">
                  <a href="#" class="partner__link">
                  <img class="partner_img" src="https://image.ibb.co/ekqdzG/codepenlogo.png">
                  </a>
                </div>
              </div>
            </div>    
          </div>
          
          <h2 class="row__sub">Community  <span class="row__sub--light">Road</span> Map</h2>
        </div>
        <div class="row row--margin row--text-center">
          <div class="col-md-8 col-sm-10 col-xs-12 row__carousel">
            <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
              <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
              <li data-target="#myCarousel" data-slide-to="1"></li>
              <li data-target="#myCarousel" data-slide-to="2"></li>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner">
              <div class="item active">
                <div class="item__content">
                  <img class="item__img" src="https://cdn.worldvectorlogo.com/logos/slack-1.svg" alt="Slack"><span class="item__name">slack</span>
                  <p class="item__description">
                    Q2 2018
                    * Assemble team and supporting personnel (completed) 
                    * Identify data center partner and cloud for co-hosting and security audit (completed) 
                    * Initiate infrastructure discussions (completed) 
                    * Form the shEOS partnership and legal entity (completed)  
                    * Set up technical infrastructure to be ready for EOS.IO launch 
                    * Host gatherings to help EOS token holders register wallets in preparation for the launch 
                    * Engage with blockchain educators to establish the shEOS scholarship initiative.
                  </p>
                </div>
                <div class="item__avatar"></div>
                <p class="item__people">Josh Doe</p>
                <p class="item__occupation">Ceo of Google</p>
              </div>
              
              <div class="item">
                <div class="item__content">
                  <img class="item__img" src="https://upload.wikimedia.org/wikipedia/commons/thumb/2/2d/Google-favicon-2015.png/150px-Google-favicon-2015.png" alt="Google"><span class="item__name">google</span>
                  <p class="item__description">
                    Q3 2018
                    * Fund our first class of shEOS scholars 
                    * Embark upon a speaking tour at large-scale events for the shEOSleadership team 
                    * Establish regional educational meet-ups 
                    * Support global EOS hackathons
                  </p>
                </div>
                <div class="item__avatar"></div>
                <p class="item__people">Mary Tompson</p>
                <p class="item__occupation">Ceo of Dribbble</a>
              </div>
              
              <div class="item">
                <div class="item__content">
                  <img class="item__img" src="https://www.hrexaminer.com/wp-content/uploads/2016/10/2016-10-11-hrexaminer-stackoverflow-6-xxl-sq-250px.png" alt="Stackoverflow"><span class="item__name">stackoverflow</span>
                  <p class="item__description">
                      Q4 2018
                      * Continue community engagement and expansion through meetups and innovative events with a focus on female involvement.
                  </p>
                </div>
                <div class="item__avatar"></div>
                <p class="item__people">Andrew Palmer</p>
                <p class="item__occupation">Ceo of Slack</p>
              </div>
              
              <div class="item">
                  <div class="item__content">
                    <img class="item__img" src="https://www.hrexaminer.com/wp-content/uploads/2016/10/2016-10-11-hrexaminer-stackoverflow-6-xxl-sq-250px.png" alt="Stackoverflow"><span class="item__name">stackoverflow</span>
                    <p class="item__description">
                        Q1 2019
                        * Develop educational content focused on attracting female developers to the EOS platform and to dissemination of information about the EOS platform.
                    </p>
                  </div>
                  <div class="item__avatar"></div>
                  <p class="item__people">Andrew Palmer</p>
                  <p class="item__occupation">Ceo of Slack</a>
                </div>
              </div>

              <!-- Left and right controls -->
              <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                <img class="carousel-control__img" src="data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTkuMS4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDMxLjQ5NCAzMS40OTQiIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDMxLjQ5NCAzMS40OTQ7IiB4bWw6c3BhY2U9InByZXNlcnZlIiB3aWR0aD0iMzJweCIgaGVpZ2h0PSIzMnB4Ij4KPHBhdGggZD0iTTEwLjI3Myw1LjAwOWMwLjQ0NC0wLjQ0NCwxLjE0My0wLjQ0NCwxLjU4NywwYzAuNDI5LDAuNDI5LDAuNDI5LDEuMTQzLDAsMS41NzFsLTguMDQ3LDguMDQ3aDI2LjU1NCAgYzAuNjE5LDAsMS4xMjcsMC40OTIsMS4xMjcsMS4xMTFjMCwwLjYxOS0wLjUwOCwxLjEyNy0xLjEyNywxLjEyN0gzLjgxM2w4LjA0Nyw4LjAzMmMwLjQyOSwwLjQ0NCwwLjQyOSwxLjE1OSwwLDEuNTg3ICBjLTAuNDQ0LDAuNDQ0LTEuMTQzLDAuNDQ0LTEuNTg3LDBsLTkuOTUyLTkuOTUyYy0wLjQyOS0wLjQyOS0wLjQyOS0xLjE0MywwLTEuNTcxTDEwLjI3Myw1LjAwOXoiIGZpbGw9IiM2Zjc5ZmYiLz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPC9zdmc+Cg==" />
              </a>
              <a class="right carousel-control" href="#myCarousel" data-slide="next">
                <img class="carousel-control__img" src="data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTkuMS4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDMxLjQ5IDMxLjQ5IiBzdHlsZT0iZW5hYmxlLWJhY2tncm91bmQ6bmV3IDAgMCAzMS40OSAzMS40OTsiIHhtbDpzcGFjZT0icHJlc2VydmUiIHdpZHRoPSIzMnB4IiBoZWlnaHQ9IjMycHgiPgo8cGF0aCBkPSJNMjEuMjA1LDUuMDA3Yy0wLjQyOS0wLjQ0NC0xLjE0My0wLjQ0NC0xLjU4NywwYy0wLjQyOSwwLjQyOS0wLjQyOSwxLjE0MywwLDEuNTcxbDguMDQ3LDguMDQ3SDEuMTExICBDMC40OTIsMTQuNjI2LDAsMTUuMTE4LDAsMTUuNzM3YzAsMC42MTksMC40OTIsMS4xMjcsMS4xMTEsMS4xMjdoMjYuNTU0bC04LjA0Nyw4LjAzMmMtMC40MjksMC40NDQtMC40MjksMS4xNTksMCwxLjU4NyAgYzAuNDQ0LDAuNDQ0LDEuMTU5LDAuNDQ0LDEuNTg3LDBsOS45NTItOS45NTJjMC40NDQtMC40MjksMC40NDQtMS4xNDMsMC0xLjU3MUwyMS4yMDUsNS4wMDd6IiBmaWxsPSIjNmY3OWZmIi8+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+Cjwvc3ZnPgo=" />
              </a>
            </div>  
          </div>
        </div>
      </div>
    </div>

    <div class="sect sect--white sect--no-padding">
      <div class="container">
        <div class="row row--center">
          <div class="col-md-3 col-xs-6 col-sm-6 partner">
            <a href="#" class="partner__link">
            <img class="partner_img" src="https://image.ibb.co/mOtHRw/fblogo.png">
            </a>
          </div>
          
          <div class="col-md-3  col-xs-6 col-sm-6 partner">
            <a href="#" class="partner__link">
            <img class="partner_img" src="https://image.ibb.co/nfpXRw/twitterlogo.png">
            </a>
          </div>
          
          <div class="col-md-3 col-xs-6 col-sm-6 partner">
            <a href="#" class="partner__link">
            <img class="partner_img" src="https://image.ibb.co/imgOYb/googlelogo.png">
            </a>
          </div>
          
          <div class="col-md-3 col-xs-6 col-sm-6 partner">
            <a href="#" class="partner__link">
            <img class="partner_img" src="https://image.ibb.co/ebGAeG/dribbblelogo.png">
            </a>
          </div>
              
          
        </div>
        <div class="row row--center">
          <div class="col-md-3 col-xs-6 col-sm-6 partner">
            <a href="#" class="partner__link">
            <img class="partner_img" src="https://image.ibb.co/npV8Yb/gitlogo.png">
            </a>
          </div>
          
          <div class="col-md-3 col-xs-6 col-sm-6 partner">
            <a href="#" class="partner__link">
            <img class="partner_img" src="https://image.ibb.co/cGyZ6w/stacklogo.png">
            </a>
          </div>
          
          <div class="col-md-3 col-xs-6 col-sm-6 partner">
            <a href="#" class="partner__link">
            <img class="partner_img" src="https://image.ibb.co/ij03zG/inlogo.png">
            </a>
          </div>
          
          <div class="col-md-3 col-xs-6 col-sm-6 partner">
            <a href="#" class="partner__link">
            <img class="partner_img" src="https://image.ibb.co/ekqdzG/codepenlogo.png">
            </a>
          </div>
        </div>
      </div>    
    </div>

    <div class="sect sect--white">
      <div class="container">
        <div class="row">
          <h1 class="row__title">
          Our blog
          </h1>
          <h2 class="row__sub">Sneak peeks from our writings</h2>
        </div>
        
        <div class="row row--margin">
          <div class="col-md-6 article-pre__col">
            <a href="#" class="article-pre ">
              <div class="article-pre__img article-pre__img--first"></div>
              <h2 class="article-pre__info">
                <span class="article-pre__cat">Protips • </span><span class="article-pre__aut"> by Ann Timothy</span> <span class="article-pre__date"> - 5 mins read</span>
              </h2>
              <h1 class="article-pre__title">How to improve analytics using few tools in Bricks<span class="article-pre__arrow--purple"> →</span></h1>
            </a>
          </div>
            <div class="col-md-6 article-pre__col">
              <a href="#" class="article-pre ">
                <div class="article-pre__img article-pre__img--second"></div>
                <h2 class="article-pre__info">
                  <span class="article-pre__cat">Pricing • </span><span class="article-pre__aut"> by Josh Ford</span> <span class="article-pre__date"> - 5 mins read</span>
                </h2>
                <h1 class="article-pre__title">Rich Thornett & Dan Coderholm about Dribbble in early 2009<span class="article-pre__arrow--purple">→</span></h1>
              </a>
            </div>    
          </div>
        <div class="row">
          <div class="col-md-6 article-pre__col">
            <a href="#" class="article-pre ">
              <div class="article-pre__img article-pre__img--fourth"></div>
              <h2 class="article-pre__info">
                <span class="article-pre__cat">Success Stories • </span><span class="article-pre__aut"> by Andrew Lincoln</span> <span class="article-pre__date"> - 5 mins read</span>
              </h2>
              <h1 class="article-pre__title">Steward Butterfield told us about his startup Slack<span class="article-pre__arrow--purple"> →</span></h1>
            </a>
          </div>
          <div class="col-md-6 article-pre__col">
            <a href="#" class="article-pre ">
              <div class="article-pre__img article-pre__img--third"></div>
              <h2 class="article-pre__info">
                <span class="article-pre__cat">Protips • </span><span class="article-pre__aut"> by Ann Timothy</span> <span class="article-pre__date"> - 5 mins read</span>
              </h2>
              <h1 class="article-pre__title">How to improve analytics using few tools in Bricks<span class="article-pre__arrow--purple"> →</span></h1>
            </a>
          </div>     
        </div>
      </div>
    </div>

    <div class="sect sect--padding-bottom">
      <div class="container">
        <div class="row">
          <h1 class="row__title">
            Contact Us 
          </h1>
          <h2 class="row__sub">Feel free to ask any questions</h2>
        </div>
        <div class="row row--margin">
          <div class="col-md-1"></div>
          <div class="col-md-4">
            <div class="contacts">
              <a href="#" class="contacts__link"><img src="https://image.ibb.co/kcVou6/path3000.png"><h1 class="contacts_title-ag">sh<span class="contacts--light">EOS</span></h1></a>
              <p class="contacts__address">
                431 Broadway, Floor 1-2<br>
                New York NY 10013<br>
                United States
              </p>
              <p class="contacts__info">
                tel. <a href="#" class="contacts__info-link">+1 234 567 890</a>
              </p>
              <p class="contacts__info">
                m. <a href="#"class="contacts__info-link">info@bricks.io</a>
              </p>
            </div>
          </div>
          <div class="col-md-6">
            <form id="contact" class="form">
              <div class="form-group">
                <select class="form__field form__select">
                  <option selected value>Choose topic*</option>
                  <option value=1>Press</option>
                  <option value=2>Developer</option>
                </select>
              </div>
              <div class="form-group">
                <div class="form__field--half">
                  <input type="text" placeholder="Name*" class="form__field form__text"></input>
                </div>
                <div class="form__field--half">
                  <input type="text" placeholder="Surname" class="form__field form__text"></input>
                </div>
              </div>
              <div class="form-group">
                <div class="form__field--half">
                  <input type="text" placeholder="Email address*" class="form__field form__text"></input>
                </div>
                <div class="form__field--half">
                  <input type="text" placeholder="Phone number" class="form__field form__text"></input>
                </div>
              </div>
              <div class="form-group">
                <textarea type="text" placeholder="Your messsage*" class="form__field form__textarea"></textarea>
                <button class="btn btn--up btn--width" type="submit">Submit</button>
              </div>
            </form>
          </div>   
          <div class="col-md-1"></div>
        </div>
      </div>
    </div>

    <div class="sect sect--violet ">
      <img src="https://image.ibb.co/fWyVtb/path3762.png" class="career-img">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <h1 class="career_title">Block the Vote!</h1>
            <h1 class="career_sub">We will pleased to have you onboard! Click below to learn how to vote.</h1>
            <a href="#" class="btn btn--white btn--width">Vote</a>
          </div>
        </div>
      </div> 
    </div>
    <!-- comienza footer -->
    <?php
    include("partials/_footer.php");
    ?>
    
  </body>
</html>