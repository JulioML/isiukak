    <header class="header">
        <div class="container header__container">
            <div class="header__logo">
            <img class="header__img" src="public/images/comq&.jpg" width="80px"> 
            <h1 class="header__title">
                <!--Com<span class="header__light">Q&</span>-->
            </h1>
            </div> 
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            </button>
            <div class="header__menu">
            <nav id="navbar" class="header__nav collapse">
                <ul class="header__elenco">
                <li class="header__el"><a href="#" class="header__link">Technology</a></li>
                <li class="header__el"><a href="#" class="header__link">Community</a></li>
                <li class="header__el"><a href="#" class="header__link">Team</a></li>
                <li class="header__el"><a href="#" class="header__link">Vision</a></li>
                <li class="header__el"><a href="#" class="header__link">News </a></li>
                <li class="header__el header__el--blue"><a href="http://localhost/Proyecto/isiukak/login.php" class="btn btn--white">Login</a></li>
                </ul>
            </nav>
            </div>
        </div>
    </header>